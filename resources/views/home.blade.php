@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Welcome {{ Auth::user()->username }}
                    <img src="{{ Auth::user()->profile_picture }}">                    
                    <div>
                    <nav class="nav">
                        <a class="nav-link active" href="#">Posts</a>
                        <a class="nav-link" href="#">Comments</a>
                    </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
